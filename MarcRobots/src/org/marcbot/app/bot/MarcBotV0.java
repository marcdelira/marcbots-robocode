package org.marcbot.app.bot;

import java.awt.Color;

import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;
import robocode.util.Utils;

public class MarcBotV0 extends Robot {
	double gunTurn;
	private int avanca = 150;
	private int countTurns = 0;

	@Override
	public void run() {

		setColors(new Color(198, 0, 0),  // body
				 new Color(211, 219, 0), // gun
				 new Color(198, 0, 0),   // radar
				 new Color(189, 252, 250), //bullet
				 new Color(189, 252, 250)); //arc radar
				
		while(true) {
			if (countTurns > 5) {
				searchEnemy();
				countTurns = 0;
			}
			ahead(avanca);
			turnLeft(60);
			countTurns++;
		}
	}
	
	private void searchEnemy() {
		stop();
		turnLeft(360);
	}

	@Override
	public void onHitWall(HitWallEvent event) {
		stop();
		back(50);
		turnLeft(30);
		resume();
	}
	
	
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		fire(3);
		countTurns = 0;
	}
	
	@Override
	public void onWin(WinEvent event) {
		stop();
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
		
	}
	
	@Override
	public void onHitByBullet(HitByBulletEvent e) {
		gunTurn = Utils.normalRelativeAngleDegrees(e.getBearing() + getHeading());
		turnRight(gunTurn);
		fire(3);
	}	

}
