package org.marcbot.app.bot;

import java.awt.Color;

import robocode.AdvancedRobot;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import robocode.TurnCompleteCondition;
import robocode.WinEvent;
import robocode.util.Utils;

public class InfiniteBot extends AdvancedRobot {
	
	@Override
	public void run() {
				
		int value = Utils.getRandom().nextInt(2);
		
		setColors(new Color(83, 2, 64),  // body
				 new Color(8, 7, 5), // gun
				 new Color(83, 2, 64),   // radar
				 new Color(199, 214, 213), //bullet
				 new Color(64, 67, 78)); //arc radar
		
		while(true) {
			setAhead(200);
			
			if (value == 0) {
				setTurnRight(30);
			} else {
				setTurnLeft(90);
			}
			waitFor(new TurnCompleteCondition(this));
			
			setAhead(200);
			value = Utils.getRandom().nextInt(2);
			if (value == 0) {
				setTurnLeft(30);				
			} else {
				setTurnRight(90);				
			}
			waitFor(new TurnCompleteCondition(this));
			
			value = Utils.getRandom().nextInt(2);
		}
	}
	
	@Override
	public void onHitWall(HitWallEvent event) {
		setStop(true);
		back(120);
		waitFor(new TurnCompleteCondition(this));
	}
	
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		fire(3);
	}
	
	@Override
	public void onWin(WinEvent event) {
		stop();
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}
}
