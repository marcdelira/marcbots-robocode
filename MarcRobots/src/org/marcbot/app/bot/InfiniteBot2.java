package org.marcbot.app.bot;

import java.awt.Color;

import robocode.AdvancedRobot;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import robocode.TurnCompleteCondition;

public class InfiniteBot2 extends AdvancedRobot {

	@Override
	public void run() {
		int direcao = 2;
		setColors(new Color(83, 2, 64), // body
				new Color(8, 7, 5), // gun
				new Color(83, 2, 64), // radar
				new Color(199, 214, 213), // bullet
				new Color(64, 67, 78)); // arc radar

		while (true) {
			setAhead(1000);
			
			if (direcao % 2 == 0) {
				setTurnRight(360);
				waitFor(new TurnCompleteCondition(this));
			} else {
				setTurnLeft(360);
				waitFor(new TurnCompleteCondition(this));
			}
			direcao++;			
		}
	}
	
	@Override
	public void onHitWall(HitWallEvent event) {
		back(50);
		turnRight(120);
//		setTurnRight(150);
//		waitFor(new TurnCompleteCondition(this));
	}
	
	@Override
	public void onScannedRobot(ScannedRobotEvent event) {
		fire(3);
	}
}
