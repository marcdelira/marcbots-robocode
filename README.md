## Projeto Robocode
### Marcello de Lira Meira

**Apresentação** <br>
[`marcbots-robocode/Apresentação/Apresentação_IA-jogos.pdf`](https://gitlab.com/marcdelira/marcbots-robocode/blob/master/Apresenta%C3%A7%C3%A3o/Apresenta%C3%A7%C3%A3o_IA-jogos.pdf) <br><br>
**Código fonte** <br>
[`marcbots-robocode/MarcRobots/src/org/marcbot/app/bot/MarcBotV0.java`](https://gitlab.com/marcdelira/marcbots-robocode/blob/master/MarcRobots/src/org/marcbot/app/bot/MarcBotV0.java)